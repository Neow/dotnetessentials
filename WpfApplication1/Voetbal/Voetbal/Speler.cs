﻿using System;

namespace Voetbal
{
    /*
Maak een klasse Speler met de volgende properties:
Naam, Voornaam, Rugnummer, en Functie (deze kan Doelman,
Verdediger, Middenvelder of Aanvaller zijn). Gebruik de 
Enumeration Spelersfunctie uit je skelet om dit te implementeren. 
*/

    public enum SpelersFunctie
    {
        Doelman = 'D',
        Verdediger = 'V',
        Middenvelder = 'M',
        Aanvaller = 'A'
    }

    public class Speler
    {
        public int stamnummer, rugnummer;
        public string voornaam, achternaam,functieVanSpeler;
 

        public Speler(int stamnummer, int rugnummer, string voornaam, string achternaam, char functieVanSpeler)  //ctor + tab voor het maken van een constructor
        {
            this.stamnummer = stamnummer;
            this.rugnummer = rugnummer;
            this.voornaam = voornaam;
            this.achternaam = achternaam;
            this.functieVanSpeler = Convert.ToString((SpelersFunctie)functieVanSpeler);
        }
    }
}