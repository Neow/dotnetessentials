﻿using System;
using System.Windows;

namespace Voetbal
{
    /// <summary>
    /// Interaction logic for RangschikkingWindow.xaml
    /// </summary>
    public partial class RangschikkingWindow : Window
    {
        private Speeldag speeldag;

        public RangschikkingWindow(Speeldag speeldag)
        {
            InitializeComponent();
            this.speeldag = speeldag;
            ToonPloegen();
        }

        public void ToonPloegen()
        {
            rangschikkingTextBox.Text = String.Empty;

            for (int i = 0; i < 8; i++)
            {
                rangschikkingTextBox.AppendText(string.Format("{0} \t {1} ", speeldag.reeks1[i].punten, speeldag.reeks1[i].ploegNaam));
                rangschikkingTextBox.AppendText(Environment.NewLine);
            }
            for (int i = 0; i < 8; i++)
            {
                rangschikkingTextBox.AppendText(string.Format("{0} \t {1} ", speeldag.reeks2[i].punten, speeldag.reeks2[i].ploegNaam));
                rangschikkingTextBox.AppendText(Environment.NewLine);
            }
        }

        private void sluitButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}