﻿using System;
using System.Collections.Generic;

namespace Voetbal
{
    // Maak een klasse Speeldag met de volgende properties: Dagnummer, PloegenReeks1,
    // PloegenReeks2, ScoreReeks1, ScoreReeks2 en Datum.PloegenReeks1 en PloegenReeks2
    // zijn generische lijsten van Ploeg objecten.ScoreReeks1 en ScoreReeks2 zijn generische lijsten van integers.

    public class Speeldag
    {
        public int dagnummer;
        public List <Ploeg> reeks1 =new List<Ploeg>();
        public List <Ploeg> reeks2 = new List<Ploeg>();
        public List<int> ScoreReeks1;
        public List<int> ScoreReeks2;
        public DateTime dag;

        public Speeldag(int dagnummer)
        {
            this.dagnummer = dagnummer;
        }

        public Speeldag(int dagnummer, List<Ploeg> reeks1, List<Ploeg> reeks2, DateTime dag )
        {
            this.dagnummer = dagnummer;
            this.reeks1 = reeks1;
            this.reeks2 = reeks2;
            this.dag = dag;
        }
    }
}