﻿using System.Windows;
using System.Windows.Controls;

namespace Voetbal
{
    /// <summary>
    /// Interaction logic for WedstrijdWindow.xaml
    /// </summary>
    public partial class WedstrijdWindow : Window
    {
        public Ploeg ploegEen;
        public Ploeg ploegTwee;

        public WedstrijdWindow(Ploeg ploegEen, Ploeg ploegTwee)
        {
            InitializeComponent();
            this.ploegEen = ploegEen;
            this.ploegTwee = ploegTwee;
            updateScore();
            ploeg1Label.Content = ploegEen.ploegNaam;
            ploeg2Label.Content = ploegTwee.ploegNaam;
            ToonSpelers();
        }

        public void ToonSpelers()
        {
            ListBoxItem temp;

            for (int i = 0; i < ploegEen.spelers.Count; i++)
            {
                temp = new ListBoxItem();
                temp.Content = string.Format("{0} {1} ", ploegEen.spelers[i].voornaam, ploegEen.spelers[i].achternaam);
                spelersPloeg1ListBox.Items.Add(temp);
                temp = new ListBoxItem();
                temp.Content = string.Format("{0} {1} ", ploegTwee.spelers[i].voornaam, ploegTwee.spelers[i].achternaam);
                spelersPloeg2ListBox.Items.Add(temp);
            }
        }

        private void updateScore()
        {
            scoreP1Label.Content = ploegEen.punten;
            scoreP2Label.Content = ploegTwee.punten;
        }

        private void spelersPloeg1ListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ploegEen.punten++;
            updateScore();
        }

        private void spelersPloeg2ListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ploegTwee.punten++;
            updateScore();
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}