﻿using System.Collections.Generic;

namespace Voetbal
{
    //Maak een klasse Ploeg met de volgende properties:
    //Stamnummer, Naam, Punten en Spelers.Deze laatste property
    //stelt een generische lijst van Speler objecten voor die
    //meespelen in de ploeg.
    //In Ploegen.txt vind je het stamnummer van de ploeg, de naam van de ploeg en de locatie terug.

    public class Ploeg
    {
        public int stamnummer, punten;
        public string ploegNaam, locatie;
        public List<Speler> spelers = new List<Speler>();

        public Ploeg(int stamnummer, string ploegNaam, string locatie)
        {
            this.stamnummer = stamnummer;
            this.ploegNaam = ploegNaam;
            this.locatie = locatie;
        }

  
    }
}