﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace Voetbal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int spelersCount = 11;
        private List<Ploeg> ploegen;
        private List<Speeldag> speeldagen;

        public MainWindow()
        {
            InitializeComponent();
            //MaakPloegenEnSpelers();
            //WedstrijdenSamenstellen();
            //BerekenDatumsSpeeldagen();
            //ToonSpeeldagen();
        }

        public void MaakPloegenEnSpelers()
        {
            List<string[]> ploegenLijst = LeesBestandIn("Ploegen.txt");
            List<string[]> spelersLijst = LeesBestandIn("Spelers.txt");
            ploegen = new List<Ploeg>();

            for (int i = 0; i < ploegenLijst.Count; i++)
            {
                ploegen.Add(new Ploeg(Int32.Parse(ploegenLijst[i][0]), ploegenLijst[i][1], ploegenLijst[i][1]));

                for (int p = i * spelersCount; p < (i * spelersCount) + spelersCount; p++)
                {
                    ploegen[i].spelers.Add(new Speler(Int32.Parse(spelersLijst[p][0]), Int32.Parse(spelersLijst[p][1]), spelersLijst[p][2], spelersLijst[p][3], spelersLijst[p][4][0]));
                }
            }
            competitie_header.IsEnabled = true;
            //MessageBox.Show(ploegen[1].spelers.Count + " ");
        }

        public void WedstrijdenSamenstellen()
        {
            speeldagen = new List<Speeldag>();

            for (int j = 0; j < 15; j++)
            {
                speeldagen.Add(new Speeldag(j + 1));

                for (int i = 0; i < ploegen.Count / 2; i++)
                {
                    speeldagen[j].reeks1.Add(ploegen[i]);
                    speeldagen[j].reeks2.Add(ploegen[i + 8]);
                }

                ploegen.Insert(7, ploegen[ploegen.Count - 1]);  //Shuffle
                ploegen.RemoveAt(ploegen.Count - 1);

                ploegen.Insert(8, ploegen[ploegen.Count - 1]);
                ploegen.RemoveAt(1);
            }
            samenstellen_header.IsEnabled = false;
            score_header.IsEnabled = true;
            rangschikking_header.IsEnabled = true;

        }

        public void BerekenDatumsSpeeldagen()  // Pakt eerste zaterdag nog niet
        {
            DateTime day = new DateTime(2015, 7, 31);
            int dagTeller = 0;
            while (speeldagen.Count == dagTeller - 1)
            {
                if (day.DayOfWeek == DayOfWeek.Saturday)
                {
                    speeldagen[dagTeller].dag = day;
                    dagTeller++;
                }
                day = day.AddDays(1);
            }
        }

        public void ToonSpeeldagen()
        {
            for (int i = 0; i < speeldagen.Count; i++)
            {
                ListBoxItem temp = new ListBoxItem();
                temp.Content = string.Format("speeldag {0}: {1} ", i + 1, speeldagen[i].dag.ToString("dd'/'MM'/'yyyy"));
                speelDagenListBox.Items.Add(temp);
            }
        }

        public List<string[]> LeesBestandIn(String bestandsNaam) //"fileName.txt"
        {
            List<string[]> result = new List<string[]>();
            StreamReader reader = null;
            string folder = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string fullPath = System.IO.Path.Combine(folder, bestandsNaam);
            try
            {
                FileStream fileStream = new FileStream(fullPath, FileMode.Open, FileAccess.Read);
                reader = new StreamReader(fileStream, System.Text.Encoding.Default);
                while (reader.Peek() > -1)
                {
                    string line = reader.ReadLine();
                    //do stuff
                    result.Add(line.Split(','));
                }

                return result;
            }
            catch (FileNotFoundException)
            { }
            catch (Exception) //create more catches for better feedback
            { }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            return result;
        }

        private void speelDagenListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (wedstrijdenListBox.Items.Count > 0) //Enkel clearen als er iets in staat?
            {
                wedstrijdenListBox.Items.Clear();
            }
            ToonWedstrijden(speeldagen[speelDagenListBox.SelectedIndex]);
        }

        public void ToonWedstrijden(Speeldag dag)
        {
            for (int i = 0; i < dag.reeks1.Count; i++)
            {
                ListBoxItem temp = new ListBoxItem();
                temp.Content = string.Format("{0} - {1}", dag.reeks1[i].ploegNaam, dag.reeks2[i].ploegNaam);
                wedstrijdenListBox.Items.Add(temp);
            }
        }

        private void wedstrijdenListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)  //Geeft foute index door
        {
            int index1 = speelDagenListBox.SelectedIndex;
            int index2 = wedstrijdenListBox.SelectedIndex;
            WedstrijdWindow spelersOverzicht = new WedstrijdWindow(speeldagen[index1].reeks1[index2], speeldagen[index1].reeks2[index2]);
            spelersOverzicht.ShowDialog(); //Geen focus op het vorig element.
        }

        private void rangschikking_header_Click(object sender, RoutedEventArgs e)
        {
            RangschikkingWindow rangschikking= new RangschikkingWindow(speeldagen[0]);
            rangschikking.ShowDialog(); 
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Inlezen_Click(object sender, RoutedEventArgs e)
        {
            MaakPloegenEnSpelers();
            ploegen_header.IsEnabled = false;
        }

        private void competitie_header_Click(object sender, RoutedEventArgs e)
        {
            WedstrijdenSamenstellen();
            ToonSpeeldagen();
        }
    }
}